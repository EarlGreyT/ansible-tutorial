# Ansible-Tutorial

An Ansible Tutorial for those who know nothing about it.

## Requirements

You need a Linux distribution and docker 1.9+

## How to run the tutorial?


### Build the Containers:
you might have to use sudo!
1. Clone this repo
2. cd <repo>/ansible-interactive-tutorial/images
3. make


### Run the Tutorial:
You'll find *tutorial.sh* inside the *ansible-interactive-tutorial* folder run it from your command line.
The skript will setup some docker containers and connect you to the ansible-tutorial container, which will be your workspace!

